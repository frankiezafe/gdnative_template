# gdnative template

## loading lib in godot

### *.gdnlib

these files contains references to libraries, it will be referenced in all object definition files

### *.gdns

these files contains definition of ONE object, you need one per class you want to import into godot

	[ext_resource path="res://bin/[ LIBRARY ].gdnlib" type="GDNativeLibrary" id=1]
	
	resource_name = "[ CLASS NAME ]"
	class_name = "[ CLASS NAME ]"

can stay the identical to you class name

therefore, if you have multiple class to load, you need one *gdns* per class, but only one *gdnlib*

## using classes in godot

there are 2 ways (maybe more?) to load your class in the engine:

### direct innheritance

* create a node in the scene tree
* attach a script to the node (a popup show up)
* browse the files in the project and select the *gdns*

### via gdscript

* attach a script to the node (a popup show up)
* create a new gdscript
* in the gdscript, do something like this:

```python
extends Sprite
var gd_script_node
func _ready():
	gd_script_node = load( "res://bin/gd_script_node.gdns" ).new()
	pass
func _process(delta):
	gd_script_node._update( delta )
	pass
```

## compilation, no headache process

just verifying that the basic example is compiling

### linux

    git clone https://gitlab.com/frankiezafe/gdnative_template.git
    cd gdnative_template
    git submodule init && git submodule update
    cd godot-cpp
    git submodule init && git submodule update
    scons platform=linux headers_dir=godot_headers generate_bindings=yes
    cd ..
    scons platform=linux

### osx

    git clone https://gitlab.com/frankiezafe/gdnative_template.git
    cd gdnative_template
    git submodule init && git submodule update
    cd godot-cpp
    git submodule init && git submodule update
    scons platform=osx headers_dir=godot_headers generate_bindings=yes
    cd ..
    scons platform=osx

### windows

#### visual studio 2017

it is a bit easier with this version

open a standard terminal with windows key & "cmd"

    git clone https://gitlab.com/frankiezafe/gdnative_template.git
    cd gdnative_template
    git submodule init && git submodule update
    cd godot-cpp
    git submodule init && git submodule update
    scons platform=windows headers_dir=godot_headers generate_bindings=yes
    cd ..
    scons platform=windows


#### visual studio 2015

open a Visual Studio command prompt, and not the standard "cmd"!

    git clone https://gitlab.com/frankiezafe/gdnative_template.git
    cd gdnative_template
    git submodule init && git submodule update
    cd godot-cpp
    git submodule init && git submodule update
    call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\amd64\vcvars64.bat"
    scons platform=windows headers_dir=godot_headers generate_bindings=yes
    cd ..
    call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\amd64\vcvars64.bat"
    scons platform=windows

and that should be ok

now open godot engine v3.0 and import the demo project, it should behave as described in the [tutorial](http://docs.godotengine.org/en/3.0/tutorials/plugins/gdnative/gdnative-cpp-example.html#using-your-gdnative-module) 

## compiling godot engine (optional but usefull to ensure your engine is compiled with the same compiler as your lib)

`cd ~/forge.godot`
 
`git clone git@github.com:godotengine/godot.git godotengine-stable`
 
important for gdnative > master is ahead of the official release!

`git checkout -b 3.0`

scons will use gcc by default on linux, if you use the "use_llvm=yes" flag for compilation of the engine, use it for gdnative also!

`scons platform=x11`

## notes

### install llvm & clang (if using "use_llvm=yes" in scons)

`sudo apt install llvm clang`

### clear scons

`scons platform=x11 --config=force`

## links

* http://docs.godotengine.org/en/3.0/tutorials/plugins/gdnative/gdnative-cpp-example.html
* https://github.com/BastiaanOlij/gdnative_cpp_example/