/* 
 * File:   gd_script_node.h
 * Author: frankiezafe
 *
 * Created on December 10, 2018, 11:37 AM
 */

#ifndef GDCUSTOMNODE_H
#define GDCUSTOMNODE_H

#include <Godot.hpp>
#include <Node.hpp>

namespace godot {

    class gd_script_node : public godot::GodotScript<Node> {
	GODOT_CLASS(gd_script_node)
        
    public:
        
	static void _register_methods();
        
        gd_script_node();
        virtual ~gd_script_node();
        
	void _update(float delta);
                
    private:
        
	float time_passed;

    };
};

#endif /* GDCUSTOMNODE_H */

