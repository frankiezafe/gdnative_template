#include "gd_script_sprite.h"

using namespace godot;

void gd_script_sprite::_register_methods() {
    register_method((char *) "_process", &gd_script_sprite::_process);
    register_method((char *) "_update", &gd_script_sprite::_update);
}

gd_script_sprite::gd_script_sprite() {
    // initialize any variables here
    time_passed = 0.0;
}

gd_script_sprite::~gd_script_sprite() {
    // add your cleanup here
}

void gd_script_sprite::_process(float delta) {

    time_passed += delta;
    Vector2 new_position = Vector2(10.0 + (10.0 * sin(time_passed * 2.0)), 10.0 + (10.0 * cos(time_passed * 1.5)));
    owner->set_position(new_position);
    std::cout << "gd_script_sprite::_process, " << time_passed << std::endl;
}

void gd_script_sprite::_update(float delta) {

    time_passed += delta;
    std::cout << "gd_script_sprite::_update, " << time_passed << std::endl;

}