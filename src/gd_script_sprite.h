#ifndef GDEXAMPLE_H
#define GDEXAMPLE_H

#include <Godot.hpp>
#include <Sprite.hpp>

namespace godot {

    class gd_script_sprite : public godot::GodotScript<Sprite> {
        GODOT_CLASS(gd_script_sprite)

    private:

        float time_passed;

    public:
        static void _register_methods();

        gd_script_sprite();
        ~gd_script_sprite();

        void _process(float delta);
        void _update(float delta);
    };

}

#endif