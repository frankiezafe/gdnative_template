/* 
 * File:   gd_script_node.cpp
 * Author: frankiezafe
 * 
 * Created on December 10, 2018, 11:37 AM
 */

#include "gd_script_node.h"

using namespace godot;

void gd_script_node::_register_methods() {
    register_method((char *) "_update", &gd_script_node::_update);
}

gd_script_node::gd_script_node() : time_passed(0) {
}

gd_script_node::~gd_script_node() {
}

void gd_script_node::_update(float delta) {
    
    time_passed += delta;
    std::cout << "gd_script_node::_update, " << time_passed << std::endl;
    
}